const canvasSketch = require( 'canvas-sketch' )
const { lerp } = require( 'canvas-sketch-util/math' )
const random = require( 'canvas-sketch-util/random' )
const palettes = require( 'nice-color-palettes' )

const settings = {
  dimensions: [ 2048, 2048 ],
  pixelsPerInch: 300,
  orientation: 'landscape',
}

const sketch = () => {
  const palette = random.pick( palettes )
  console.log( palette )

  const createGrid = () => {
    const points = []
    const count = 100
    const foregroundPalette = palette.slice( 0, 4 )

    for ( let x = 0; x < count; x++ ) {
      for ( let y = 0; y < count; y++ ) {
        const u = x / ( count - 1 )
        const v = y / ( count - 1 )
        points.push( {
          color: random.pick( foregroundPalette ),
          size: random.gaussian(),
          pos: [ u, v ],
        } )
      }
    }

    return points
  }

  random.setSeed( 'jojoto' )
  const points = createGrid().filter( () => random.value() < 0.5 )
  const margin = 200

  return ( { context, width, height } ) => {
    context.fillStyle = palette[4]
    context.fillRect( 0, 0, width, height )

    points.forEach( ( { color, size, pos: [ u, v ] } ) => {
      const x = lerp( margin, width - margin, u )
      const y = lerp( margin, width - margin, v )

      context.beginPath()
      context.arc( x, y, Math.abs( size * 13 ), 0, Math.PI * 2, false )
      context.fillStyle = color
      context.fill()
    } )
  }
}

canvasSketch( sketch, settings )
