const canvasSketch = require( 'canvas-sketch' )
const { lerp } = require( 'canvas-sketch-util/math' )
const random = require( 'canvas-sketch-util/random' )
const palettes = require( 'nice-color-palettes' )

const settings = {
  dimensions: [ 2048, 2048 ],
  // pixelsPerInch: 300,
  orientation: 'landscape',
}

const sketch = () => {
  const palette = random.pick( palettes )
  console.log( palette )

  const createGrid = () => {
    const points = []
    const count = 30
    const foregroundPalette = palette.slice( 0, 4 )

    for ( let x = 0; x < count; x++ ) {
      for ( let y = 0; y < count; y++ ) {
        const u = x / ( count - 1 )
        const v = y / ( count - 1 )
        const size = Math.abs( random.noise2D( u,v ) ) 

        console.log( 'SIZE:', size )

        points.push( {
          color: random.pick( foregroundPalette ),
          size,
          rotation: random.noise2D( u,v ),
          pos: [ u, v ],
        } )
      }
    }

    return points
  }

  const points = createGrid().filter( () => random.value() > 0.7 )
  const margin = 200

  return ( { context, width, height } ) => {
    context.fillStyle = palette[4]
    context.fillRect( 0, 0, width, height )

    points.forEach( ( { 
      color,
      size, 
      rotation,
      pos: [ u, v ] 
    } ) => {
      const x = lerp( margin, width - margin, u )
      const y = lerp( margin, width - margin, v )

      context.save()
      context.fillStyle = color
      context.font = `${ size * width  / 8}px "Operator Mono"`
      context.translate( x,y )
      context.rotate( rotation )
      context.fillText( '=', 0, 0 )
      context.restore()
    } )
  }
}

canvasSketch( sketch, settings )
