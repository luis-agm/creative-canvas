const canvasSketch = require( 'canvas-sketch' )

const settings = {
  dimensions: 'A4',
  pixelsPerInch: 300,
  orientation: 'landscape',
  units: 'cm'
}

const sketch = () => {
  return ( { context, width, height } ) => {
    context.fillStyle = 'violet'
    context.fillRect( 0, 0, width, height )

    context.beginPath()
    context.arc( width / 2, height / 2, 5, 0, Math.PI * 2, false )
    context.fillStyle = 'red'
    context.fill()
    context.lineWidth = 2
    context.strokeStyle = 'blue'
    context.stroke()
  }
}

canvasSketch( sketch, settings )
