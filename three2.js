// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require( 'three' )
const { times } = require( 'lodash' )
const canvasSketch = require( 'canvas-sketch' )
const random = require( 'canvas-sketch-util/random' )
const palettes = require( 'nice-color-palettes' )
const uuid = require( 'uuid' )
const BezierEasing = require( 'bezier-easing' )

// Cool seed
// c3ee0cb3-4f49-422a-9cc3-b01f9c020505

random.setSeed(  uuid.v4() )
console.log( random.getSeed() )

const settings = {
  dimensions:  [ 512,512 ],
  fps: 24,
  duration: 4,
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: 'webgl'
}

const sketch = ( { context } ) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer( {
    canvas: context.canvas
  } )

  // WebGL background color
  renderer.setClearColor( '#FFF', 1 )

  // Setup a camera
  const camera = new THREE.OrthographicCamera( )
  camera.position.set( 0, 0, -4 )
  camera.lookAt( new THREE.Vector3() )

  // Setup your scene
  const scene = new THREE.Scene()

  // Setup a geometry
  const geometry = new THREE.BoxGeometry( 1, 1, 1 )

  /*   // Texture
  const texture = new THREE.TextureLoader().load( 'fed.jpg' ) */
  
  const palette = random.pick( palettes )

  // Setup a mesh with geometry + material
  times( 50, () => {
    const mesh = new THREE.Mesh( geometry, new THREE.MeshStandardMaterial( {
      color: random.pick( palette ),
    } ) )
    mesh.position.set( random.range( -1,1 ), random.range( -1,1 ), random.range( -1,1 ) )
    mesh.scale.set( random.range( -0.5,0.5 ), random.range(  -0.5,0.5 ), random.range(  -0.5,0.5 ) )
    scene.add( mesh )
  } )

  // Light
  const light = new THREE.DirectionalLight( 'white', 2.5 )
  light.position.set( 2,2,4 )
  scene.add( light )

  // draw each frame
  return {
    // Handle resize events here
    resize( { pixelRatio, viewportWidth, viewportHeight } ) {
      renderer.setPixelRatio( pixelRatio )
      renderer.setSize( viewportWidth, viewportHeight, false )

      const aspect = viewportWidth / viewportHeight

      // Ortho zoom
      const zoom = 2

      // Bounds
      camera.left = -zoom * aspect
      camera.right = zoom * aspect
      camera.top = zoom
      camera.bottom = -zoom

      // Near/Far
      camera.near = -100
      camera.far = 100

      // Set position & look at world center
      camera.position.set( zoom, zoom, zoom )
      camera.lookAt( new THREE.Vector3() )

      // Update the camera
      camera.updateProjectionMatrix()
    },
    // Update & render your scene here
    render( { playhead } ) {
      const easingFn = BezierEasing( .99,0,.42,1.07 )

      const t =  playhead * Math.PI * 2 
      scene.rotation.y = easingFn( t ) 
      renderer.render( scene, camera )
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload() {
      renderer.dispose()
    }
  }
}

canvasSketch( sketch, settings )
