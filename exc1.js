const canvasSketch = require( 'canvas-sketch' )
const { lerp } = require( 'canvas-sketch-util/math' )
const random = require( 'canvas-sketch-util/random' )
const palettes = require( 'nice-color-palettes' )

const settings = {
  dimensions: [ 2048, 2048 ],
  // pixelsPerInch: 300,
  orientation: 'landscape',
}

random.setSeed( 'balls' )

const connectTwoPoints = ( context,points, margin, width ) => {
  const { pos: p1 } = random.pick( points )
  const [ p1x, p1y ] = p1.map( ( el ) => {
    return lerp( margin, width - margin, el )
  } )

  const { pos: p2 } = random.pick( points )
  const [ p2x, p2y ] = p2.map( ( el ) => {
    return lerp( margin, width - margin, el )
  } )

  context.strokeStyle = 'black'
  context.moveTo( p1x, p1y )
  context.lineTo( p2x,p2y )
  context.stroke()

  return [ [ p1x, p1y ], [ p2x, p2y ] ]
}


const sketch = () => {
  const palette = random.pick( palettes )

  const createGrid = () => {
    const points = []
    const count = 6
    const foregroundPalette = palette.slice( 0, 4 )

    for ( let x = 0; x < count; x++ ) {
      for ( let y = 0; y < count; y++ ) {
        const u = x / ( count - 1 )
        const v = y / ( count - 1 )

        points.push( {
          color: random.pick( foregroundPalette ),
          size: 10,
          pos: [ u, v ],
        } )
      }
    }

    return points
  }

  const points = createGrid()
  const margin = 200

  return ( { context, width, height } ) => {
    context.fillStyle = palette[4]
    context.fillRect( 0, 0, width, height )

    points.forEach( ( { 
      color,
      size, 
      pos: [ u, v ] 
    } ) => {
      const x = lerp( margin, width - margin, u )
      const y = lerp( margin, width - margin, v )

      context.beginPath()
      context.arc( x, y, Math.abs( size  ), 0, Math.PI * 2, false )
      context.fillStyle = color
      context.fill()
      context.closePath()
    } )

    context.beginPath()
    context.lineWidth = 19

    const [ p1, p2 ] = connectTwoPoints( context,points, margin, width )
    const separation = p2[0] - p1[0]
    const p3 = [ p1[0] ,p1[1] + ( separation  ) ]
    const p4 = [ p2[0], p2[1] + ( separation * 3 ) ]

    context.moveTo( ...p2 )
    context.lineTo( ...p4 )
    context.lineTo( ...p3 )
    context.lineTo(  ...p1 )
    context.fillStyle =  palette[2]
    context.fill()
    context.closePath()
    context.strokeStyle = palette[1]
    context.stroke()
  }
}

canvasSketch( sketch, settings )
